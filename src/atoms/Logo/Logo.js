import React from 'react'
import logo from '../../de-bruin_metaalwerken.png'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
    logo: {
        [theme.breakpoints.down('xs')]: {
            maxWidth: 164
        }
    },
    logoImage: {
        display: 'block'
    }
}))

const Logo = () => {
    const classes = useStyles()

    return (
        <div className={classes.logo}>
            <img src={logo}
                className={classes.logoImage}
                alt="De Bruin: Metaalwerken"
                title="De Bruin: Metaalwerken"
                width="200" />
        </div>
    )
}

export default Logo
