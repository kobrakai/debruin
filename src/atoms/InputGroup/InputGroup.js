import React from "react";
import TextField from "@material-ui/core/TextField";

const InputGroup = (props) => (
  <div className="input-group" key={props.id}>
    <TextField
      required={props.required}
      id={props.id}
      name={props.id}
      type={props.type}
      label={props.label}
      value={props.value}
      onChange={props.onChange}
      onBlur={props.onBlur}
      autoComplete={props.type === "email" ? "email" : null}
      margin="normal"
      variant="outlined"
      helperText={props.message}
      fullWidth
      error={!props.valid}
    />
  </div>
);

export default InputGroup;
