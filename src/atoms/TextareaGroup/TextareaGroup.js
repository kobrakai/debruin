import React from "react";
import TextField from "@material-ui/core/TextField";

const TextareaGroup = (props) => (
  <div className="textarea-group" key={props.id}>
    <TextField
      multiline
      required={props.required}
      id={props.id}
      name={props.id}
      type={props.type}
      label={props.label}
      value={props.value}
      onChange={props.onChange}
      rows="4"
      margin="normal"
      variant="outlined"
      fullWidth
    />
  </div>
);

export default TextareaGroup;
