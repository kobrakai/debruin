import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { animated, useSpring, config } from "react-spring";
import Fab from "@material-ui/core/Fab";

const useStyles = makeStyles((theme) => ({
  icon: {
    flex: "0 1 auto",
    marginRight: 8,
  },
  label: {
    flex: "1 1 auto",
  },
  button: {
    display: "flex",
    minWidth: 124,
    color: "white",
    transition: "background .3s ease-out, box-shadow .3s ease-out",
    backgroundImage: theme.palette.background.gradient,
  },
  buttonActive: {
    boxShadow: "none",
    backgroundImage: theme.palette.background.gradientTransparent,
    backgroundColor: "transparent",
    "&:hover, &:active, &:focus": {
      backgroundColor: "transparent",
    },
  },
}));

const closedState = {
  top: "translate(2, 5) rotate(0)",
  center: "translate(2, 11) rotate(0)",
  bottom: "translate(2, 17) rotate(0)",
};

const openedState = {
  top: "translate(6, 4) rotate(45)",
  center: "translate(-22, 11) rotate(0)",
  bottom: "translate(4, 18) rotate(-45)",
};

const MenuIcon = ({ isOpen, color = "black" }) => {
  const classes = useStyles();
  const { top, center, bottom } = useSpring({
    to: isOpen ? openedState : closedState,
    config: config.stiff,
  });

  return (
    <Fab
      className={
        isOpen ? `${classes.button} ${classes.buttonActive}` : classes.button
      }
      variant="extended"
      aria-label="Navigatie"
    >
      <svg
        xmlns="https://www.w3.org/2000/svg"
        className={classes.icon}
        width="24"
        height="24"
        viewBox="0 0 24 24"
        fill={color}
      >
        <animated.rect width="20" height="2" transform={top} />
        <animated.rect width="20" height="2" transform={center} />
        <animated.rect width="20" height="2" transform={bottom} />
      </svg>

      <span className={classes.label}>{isOpen ? "Sluiten" : "Menu"}</span>
    </Fab>
  );
};

export default MenuIcon;
