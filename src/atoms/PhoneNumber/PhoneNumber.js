import React from "react";
import Button from "@material-ui/core/Button";
import PhoneIcon from "@material-ui/icons/Phone";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  button: {
    minWidth: 0,
    [theme.breakpoints.down("xs")]: {
      fontSize: 10,
    },
  },
  icon: {
    [theme.breakpoints.up("xs")]: {
      marginRight: ".8rem",
    },
    [theme.breakpoints.down("xs")]: {
      display: "none",
    },
  },
}));

const PhoneNumber = () => {
  const classes = useStyles();

  return (
    <Button
      className={classes.button}
      href="tel:0636322854"
      variant="contained"
      color="primary"
    >
      <PhoneIcon className={classes.icon} color="inherit" fontSize="small" />
      <span>{process.env.REACT_APP_PHONE}</span>
    </Button>
  );
};

export default PhoneNumber;
