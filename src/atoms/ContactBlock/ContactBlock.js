import React from "react";

import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import Divider from "@material-ui/core/Divider";

import blue from "@material-ui/core/colors/blue";
import teal from "@material-ui/core/colors/teal";

import PhoneIcon from "@material-ui/icons/Phone";
import MailIcon from "@material-ui/icons/Mail";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faWhatsapp } from "@fortawesome/free-brands-svg-icons/faWhatsapp";
import { faInstagram } from "@fortawesome/free-brands-svg-icons/faInstagram";
import { faLinkedinIn } from "@fortawesome/free-brands-svg-icons/faLinkedinIn";

const ContactBlock = () => {
  const instagramStyles = {
    background: `
            radial-gradient(circle farthest-corner at 35% 90%, #fec564, transparent 50%), 
            radial-gradient(circle farthest-corner at 0 140%, #fec564, transparent 50%),
            radial-gradient(ellipse farthest-corner at 0 -25%, #5258cf, transparent 50%),
            radial-gradient(ellipse farthest-corner at 20% -50%, #5258cf, transparent 50%),
            radial-gradient(ellipse farthest-corner at 100% 0, #893dc2, transparent 50%),
            radial-gradient(ellipse farthest-corner at 60% -20%, #893dc2, transparent 50%),
            radial-gradient(ellipse farthest-corner at 100% 100%, #d9317a, transparent),
            linear-gradient(#6559ca, #bc318f 30%, #e33f5f 50%, #f77638 70%, #fec66d 100%)
        `,
  };

  const onMail = (event) => {
    event.preventDefault();
    window.location = `mailto: ${process.env.REACT_APP_EMAIL}`;
  };

  const onPhone = (event) => {
    event.preventDefault();
    window.location = `tel: ${process.env.REACT_APP_PHONE}`;
  };

  const onWhatsapp = (event) => {
    event.preventDefault();
    window.open(`https://wa.me/${process.env.REACT_APP_WHATSAPP}`, "_blank");
  };

  const onInstagram = (event) => {
    event.preventDefault();
    window.open(
      `https://www.instagram.com/${process.env.REACT_APP_INSTAGRAM}/`,
      "_blank"
    );
  };

  const onLinkedin = (event) => {
    event.preventDefault();
    window.open(
      `https://www.linkedin.com/in/${process.env.REACT_APP_LINKEDIN}/`,
      "_blank"
    );
  };

  return (
    <List>
      <ListItem button onClick={onWhatsapp}>
        <ListItemAvatar>
          <Avatar style={{ backgroundColor: "#25d366" }}>
            <FontAwesomeIcon icon={faWhatsapp} />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary="Contact via chat"
          secondary="Chatten met WhatsApp"
        />
      </ListItem>

      <Divider variant="inset" component="li" />

      <ListItem button onClick={onInstagram}>
        <ListItemAvatar>
          <Avatar style={instagramStyles}>
            <FontAwesomeIcon icon={faInstagram} />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Linkedin" secondary="Bekijk meer op Instagram" />
      </ListItem>

      <Divider variant="inset" component="li" />

      <ListItem button onClick={onLinkedin}>
        <ListItemAvatar>
          <Avatar style={{ backgroundColor: "#2867B2" }}>
            <FontAwesomeIcon icon={faLinkedinIn} />
          </Avatar>
        </ListItemAvatar>
        <ListItemText primary="Linkedin" secondary="Bekijk meer op Linkedin" />
      </ListItem>

      <Divider variant="inset" component="li" />

      <ListItem button onClick={onPhone}>
        <ListItemAvatar>
          <Avatar style={{ backgroundColor: blue[700] }}>
            <PhoneIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={process.env.REACT_APP_PHONE}
          secondary="Bel voor meer informatie"
        />
      </ListItem>

      <Divider variant="inset" component="li" />

      <ListItem button onClick={onMail}>
        <ListItemAvatar>
          <Avatar style={{ backgroundColor: teal[500] }}>
            <MailIcon />
          </Avatar>
        </ListItemAvatar>
        <ListItemText
          primary={process.env.REACT_APP_EMAIL}
          secondary="Stuur een e-mail"
        />
      </ListItem>
    </List>
  );
};

export default ContactBlock;
