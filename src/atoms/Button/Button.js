import React from "react";
import MaterialButton from "@material-ui/core/Button";

const Button = ({ title, color, disabled, onClick, ...rest }) => (
  <MaterialButton
    variant="contained"
    color={color ? color : "primary"}
    disabled={disabled !== null ? disabled : false}
    onClick={onClick !== null ? onClick : null}
    {...rest}
  >
    {title}
  </MaterialButton>
);

export default Button;
