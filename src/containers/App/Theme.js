import teal from '@material-ui/core/colors/teal'

const theme = {
  palette: {
      type: 'light',
      primary: {
        main: '#b03a20'
      },
      secondary: {
        main: teal[500]
      },
      background: {
        gradient: 'linear-gradient(45deg, #b03a20 30%, #C7531A 90%)',
        gradientTransparent: 'linear-gradient(45deg, transparent 30%, transparent 90%)'              
      },
      contrastThreshold: 3,
      tonalOffset: 0.2
  },
  overrides: {
    MuiButton: {
      containedPrimary: {
        backgroundImage: 'linear-gradient(45deg, #b03a20 30%, #C7531A 90%)',
        borderRadius: 0,
        border: 0,
        color: 'white',
        height: 40,
        padding: '0 10px',
        minWidth: 200,
        boxShadow: '0 5px 18px 0 rgba(176, 58, 32, .4), 0 2px 8px 0 rgba(199, 83, 26, .2)',

        "&$disabled": {
          opacity: 0.4
        }
      }
    },
    MuiCard: {
      root: {
        borderBottom: '10px solid #b03a20'
      }
    }
  }
}

export default theme