import React, { useState, useEffect, useCallback } from "react";
import axios from "axios";

import CssBaseline from "@material-ui/core/CssBaseline";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

import "./styles.css";
import Theme from "./Theme";

import {
  Header,
  Navigation,
  Hero,
  About,
  Contact,
  Portfolio,
  Footer,
} from "../../components";

const App = () => {
  const [theme, setTheme] = useState(Theme),
    [themeType, setThemeType] = useState("light"),
    date = new Date();

  const getThemeType = useCallback(() => {
    setThemeType(
      date.getHours() < 20 && date.getHours() > 7 ? "light" : "dark"
    );

    axios({
      method: "post",
      url:
        "https://api.sunrise-sunset.org/json?lat=52.090736&lng=5.121420&date=today&formatted=0",
      headers: { "content-type": "application/json" },
    })
      .then((response) => {
        if (response.data) {
          const sunset = new Date(response.data.results.sunset),
            sunrise = new Date(response.data.results.sunrise);

          setThemeType(sunrise < date && sunset > date ? "light" : "dark");
        }
      })
      .catch((error) => {
        console.log("Sunrise-sunset API unavailable");
      });
  }, [date]);

  useEffect(() => {
    getThemeType();

    if (theme.palette.type !== themeType) {
      setTheme({
        ...theme,
        palette: {
          ...theme.palette,
          type: themeType,
        },
      });
    }
  }, [getThemeType, theme, themeType]);

  return (
    <>
      <MuiThemeProvider theme={createMuiTheme(theme)}>
        <CssBaseline />
        <Header />
        <Navigation />
        <Hero />
        <About />
        <Contact />
        <Portfolio />
        <Footer />
      </MuiThemeProvider>
    </>
  );
};

export default App;
