import React from 'react'
import { Logo, PhoneNumber } from '../../atoms'
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import useScrollTrigger from '@material-ui/core/useScrollTrigger'
import Slide from '@material-ui/core/Slide'
import Container from '@material-ui/core/Container'
import Grid from '@material-ui/core/Grid'

const Header = (props) => {
    const HideOnScroll = (props) => {
        const { children, window } = props;
        const trigger = useScrollTrigger({ target: window ? window() : undefined })

        return (
            <Slide appear={false} direction="down" in={!trigger}>
                {children}
            </Slide>
        )
    }

    return (
        <>
            <HideOnScroll {...props}>
                <AppBar color="default">
                    <Toolbar disableGutters>
                        <Container maxWidth="lg">
                            <Grid container justify="space-between" alignItems="center">
                                <Grid item>
                                    <Logo />
                                </Grid>

                                <Grid item>
                                    <PhoneNumber />
                                </Grid>
                            </Grid>
                        </Container>
                    </Toolbar>
                </AppBar>
            </HideOnScroll>
            <Toolbar />
        </>
    )
}

Header.propTypes = {
    HideOnScroll: PropTypes.shape({     
        children: PropTypes.element.isRequired,
        window: PropTypes.func
    })
}

export default Header
