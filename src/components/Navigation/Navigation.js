import React, { useState } from 'react'
import { MenuIcon } from '../../atoms'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles(theme => ({
    button: {
        position: 'fixed',
        right: 20,
        bottom: 20,
        zIndex: 1300
    },
    menu: {
        pointerEvents: 'none',
        position: 'fixed',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        opacity: 0,
        backgroundImage: theme.palette.background.gradient,
        clipPath: 'circle(22px at calc(100% - 75px) calc(100% - 44px))',
        transition: 'clip-path .3s ease-out',
        zIndex: 1200
    },
    menuActive: {
        pointerEvents: 'all',
        clipPath: 'circle(200% at calc(100% - 75px) calc(100% - 44px))'
    },
    menuShown: {
        opacity: 1
    },
    menuList: {
        maxWidth: 320,
        margin: '25% auto 0',
        padding: 0,
        listStyle: 'none',
        textAlign: 'center'
    },
    menuLink: {
        display: 'block',
        padding: '8px 0',
        color: 'white',
        borderRadius: 4,
        textDecoration: 'none',
        '&:hover': {
            backgroundColor: 'rgba(255, 255, 255, .15)'
        }
    }
}))

const Navigation = () => {
    const [isOpen, setIsOpen] = useState(false),
          [overlay, setOverlay] = useState(false),
          overlayTimeout = false,
          classes = useStyles()

    const toggleOpen = (event) => {
        setIsOpen(!isOpen)
        toggleOverlay()
    }

    const toggleOverlay = (event) => {
        clearTimeout(overlayTimeout)

        if (isOpen) {
            setTimeout(() => {
                setOverlay(false)
            }, 300);
        } else {
            setOverlay(true)
        }
    }

    const navigationClass = classes.menu,
          openClass = isOpen ? ` ${classes.menuActive}` : '',
          shownClass = overlay ? ` ${classes.menuShown}` : '';


    return (
        <>
            <div className={classes.button} onClick={toggleOpen}>
                <MenuIcon isOpen={isOpen} color={'white'} />
            </div>

            <nav className={`${navigationClass}${openClass}${shownClass}`}>
                <ul className={classes.menuList} onClick={toggleOpen}>
                    <Typography variant="h4" component="li">
                        <a className={classes.menuLink} href="#root">Home</a>
                    </Typography>
                    <Typography variant="h4" component="li">
                        <a className={classes.menuLink} href="#about">Over</a>
                    </Typography>
                    <Typography variant="h4" component="li">
                        <a className={classes.menuLink} href="#contact">Contact</a>
                    </Typography>
                    <Typography variant="h4" component="li">
                        <a className={classes.menuLink} href="#portfolio">Portfolio</a>
                    </Typography>
                </ul>
            </nav>
        </>
    )
}

export default Navigation