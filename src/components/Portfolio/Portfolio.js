import React, { useState } from "react";
import Lightbox from "react-image-lightbox";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import PortfolioItems, { PortfolioImages } from "./PortfolioItems";

import "react-image-lightbox/style.css";

const useStyles = makeStyles({
  wrapper: {
    paddingTop: 30,
  },
  card: {
    cursor: "pointer",
    height: "100%",
  },
  media: {
    height: 280,
    backgroundPosition: "top center",
  },
  row: {
    marginBottom: "20px",
  },
});

const Portfolio = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [photoIndex, setPhotoIndex] = useState(false);
  const classes = useStyles();
  const images = PortfolioImages;

  return (
    <section id="portfolio" className={classes.wrapper}>
      <Container maxWidth="lg">
        <Typography variant="h4" component="h3" paragraph>
          Portfolio
        </Typography>

        <Grid className={classes.row} container spacing={4} direction="row">
          {PortfolioItems.map((item, index) => (
            <Grid item xs={12} md={item.size} key={item.id}>
              <Card className={classes.card}>
                <CardMedia
                  className={classes.media}
                  image={item.image}
                  title={item.title}
                  onClick={() => {
                    setIsOpen(true);
                    setPhotoIndex(index);
                  }}
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    {item.title}
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    {item.text}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>

      {isOpen && (
        <Lightbox
          mainSrc={images[photoIndex]}
          nextSrc={images[(photoIndex + 1) % images.length]}
          prevSrc={images[(photoIndex + images.length - 1) % images.length]}
          onCloseRequest={() => setIsOpen(false)}
          onMovePrevRequest={() =>
            setPhotoIndex((photoIndex + images.length - 1) % images.length)
          }
          onMoveNextRequest={() =>
            setPhotoIndex((photoIndex + 1) % images.length)
          }
        />
      )}
    </section>
  );
};

export default Portfolio;
