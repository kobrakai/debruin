import item1 from "./portfolio-item-1.jpg";
import item2 from "./portfolio-item-2.jpg";
import item3 from "./portfolio-item-3.jpg";
import item4 from "./portfolio-item-4.jpg";
import item5 from "./portfolio-item-5.jpg";
import item6 from "./portfolio-item-6.jpg";
import item7 from "./portfolio-item-7.jpg";
import item8 from "./portfolio-item-8.jpg";
import item9 from "./portfolio-item-9.jpg";
import item10 from "./portfolio-item-10.jpg";
import item11 from "./portfolio-item-11.jpg";
import item12 from "./portfolio-item-12.jpg";
import item13 from "./portfolio-item-13.jpg";
import item14 from "./portfolio-item-14.jpg";
import item15 from "./portfolio-item-15.jpg";
import item16 from "./portfolio-item-16.jpg";
import item17 from "./portfolio-item-17.jpg";
import item18 from "./portfolio-item-18.jpg";
import item19 from "./portfolio-item-19.jpg";
import item20 from "./portfolio-item-20.jpg";

export const PortfolioImages = [
  item1,
  item2,
  item3,
  item4,
  item5,
  item6,
  item7,
  item8,
  item9,
  item10,
  item11,
  item12,
  item13,
  item14,
  item15,
  item16,
  item17,
  item18,
  item19,
  item20,
];

export const PortfolioItems = [
  {
    id: 1,
    size: 4,
    image: item1,
    title: "Proefzitten",
    text: "Even zelf testen hoe het zit voor de tafel geleverd wordt.",
  },
  {
    id: 2,
    size: 4,
    image: item2,
    title: "Overkapping",
    text: "Overkapping met volledig stalen constructie en riante lichtstraat.",
  },
  {
    id: 3,
    size: 4,
    image: item3,
    title: "TIG laswerkzaamheden",
    text:
      "Op ZZP basis ingeleend door de Bolsius fabriek in verband met reparatie- en aanpassingswerk kaarsvet silos.",
  },
  {
    id: 4,
    size: 4,
    image: item4,
    title: "Tafel renovatie",
    text:
      "Het blad vol herinneringen is nieuw leven ingeblazen door middel van een slanke poot met robuuste tussenligger.",
  },
  {
    id: 5,
    size: 4,
    image: item5,
    title: "Frans eiken...",
    text: "...gecombineerd met stalen onderstel. Maatwerk voor klant.",
  },
  {
    id: 6,
    size: 4,
    image: item6,
    title: "Overkapping",
    text:
      "Overkapping afgewerkt met verlichting en stroompunten. Antraciet en weerbestendig gelakt.",
  },
  {
    id: 7,
    size: 4,
    image: item7,
    title: "Robuust en moderne eettafel met bijpassende bank",
    text:
      "Rustiek Frans eikenhout in combinatie met een stevig stalen onderstel.",
  },
  {
    id: 8,
    size: 4,
    image: item8,
    title: "Epoxy blad",
    text:
      "In opdracht voor collega interieur ontwerpen en meubelmaker een epoxy blad gegoten en later gemonteerd op bij passend stalen onderstel.",
  },
  {
    id: 9,
    size: 4,
    image: item9,
    title: 'De "M" van Mam & Mie...',
    text: "...verwerkt in het onderstel van de tafel in de showroom.",
  },
  {
    id: 10,
    size: 4,
    image: item10,
    title: "Maatwerk trap in opdracht van particulieren klant",
    text: "Vlizotrap vervangen door een vaste trap",
  },
  {
    id: 11,
    size: 4,
    image: item11,
    title: "Eigen ontwerp op basis van wensen klant",
    text:
      "Dubbele bladen gemonteerd op zowel een dragend als een hangend onderstel/frame.",
  },
  {
    id: 12,
    size: 4,
    image: item12,
    title: "Geen brug te ver",
    text: "Een nieuwe entree over de wadi op het bedrijventerrein.",
  },
  {
    id: 13,
    size: 4,
    image: item13,
    title: "Olijfhouten bijzettafel",
    text:
      "Olijfhouten wortel met blank stalen poot, afgewerkt met olijfhouten bladen.",
  },
  {
    id: 14,
    size: 4,
    image: item14,
    title: "Strak robuust massief Frans eiken",
    text:
      "Gecomplimenteerd met een straks afgeschuurd onderstel, simpel maar chic resultaat; less is more.",
  },
  {
    id: 15,
    size: 4,
    image: item15,
    title: "Tafel poten...",
    text: "...een alfabet aan mogelijkheden van diverse stalen.",
  },
  {
    id: 16,
    size: 4,
    image: item16,
    title: "Replica van het originelen jaren 30 rooster",
    text: "Geheel in stijl van de woning.",
  },
  {
    id: 17,
    size: 4,
    image: item17,
    title: "Framewerk voor hippe vakken kast",
    text:
      "Staal framewerk later afgewerkt met eikenblad gedetailleerd met epoxy belijning.",
  },
  {
    id: 18,
    size: 4,
    image: item18,
    title: "Zware stalen tafelpoten",
    text: "Van a tot z afgewerkt en geleverd.",
  },
  {
    id: 19,
    size: 4,
    image: item19,
    title: "Diverse aanpassingen op platte kar",
    text:
      "De dubbelasser omgebouwd naar aanhanger met kraan en nieuwe opbouw tbc bevestiging.",
  },
  {
    id: 20,
    size: 4,
    image: item20,
    title: "Work in progress",
    text: "Hou de website in de gaten voor het vervolg!",
  },
];

export default PortfolioItems;
