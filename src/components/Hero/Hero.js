import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import heroImage from './hero-banner.jpg'

const useStyles = makeStyles(theme => ({
    wrapper: {
        position: 'relative',
        marginTop: 40,
        [theme.breakpoints.up('md')]: {
            paddingLeft: 50,
            height: 500
        },
        [theme.breakpoints.down('sm')]: {
            paddingLeft: 20,
            height: 250,
            marginBottom: 80,
        }
    },
    image: {
        width: '100%',
        height: '100%',
        backgroundImage: `url('${heroImage}')`,
        backgroundSize: 'cover',
        backgroundPosition: 'center right'
    },
    content: {
        position: 'absolute',
        left: '0',
        maxWidth: 320,
        backgroundImage: theme.palette.background.gradient,
        color: 'white',
        [theme.breakpoints.up('md')]: {
            top: 50,
            padding: 30

        },
        [theme.breakpoints.down('sm')]: {
            bottom: -50,
            padding: 10
        }
    }
}))

const Hero = () => {
    const classes = useStyles()

    return (
        <Container maxWidth="lg">
            <div className={classes.wrapper}>
                <div className={classes.image}></div>

                <Paper className={classes.content}>
                    <Typography variant="h4" component="h1">
                        {process.env.REACT_APP_NAME}

                        <Typography variant="h6" component="div">
                            {process.env.REACT_APP_SLOGAN}
                        </Typography>
                    </Typography>
                </Paper>
            </div>
        </Container>
    )
}

export default Hero