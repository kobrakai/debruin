import React from "react";
import Container from "@material-ui/core/Container";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import portret from "./portret.jpg";

const useStyles = makeStyles((theme) => ({
  frame: {
    display: "inline-block",
    padding: ".4rem",
    border: `2px solid ${theme.palette.primary.main}`,
    borderRadius: "12rem",
    [theme.breakpoints.up("sm")]: {
      display: "block",
      width: "12rem",
      height: "12rem",
      marginLeft: "1rem",
      float: "right",
    },
    [theme.breakpoints.down("xs")]: {
      display: "block",
      width: 140,
      height: 140,
      margin: "0 auto 20px",
    },
  },
  photo: {
    width: "100%",
    height: "100%",
    borderRadius: "50%",
    backgroundImage: theme.palette.background.gradient,
    padding: 5,
    objectFit: "cover",
    objectPosition: "top left",
  },
}));

const About = () => {
  const classes = useStyles();

  return (
    <section id="about" className="about">
      <Container maxWidth="lg">
        <Paper style={{ padding: "30px", marginTop: "30px" }}>
          <Typography variant="h4" component="h3" paragraph>
            Over
          </Typography>

          <div className={classes.frame}>
            <img
              className={classes.photo}
              src={portret}
              alt="Rick de Bruin"
              title="Rick de Bruin"
            />
          </div>

          <Typography variant="body1" component="p" paragraph>
            Mijn naam is Rick de Bruin en van beroep ben ik ijzerwerker. In mijn
            kinderjaren kwam ik al met enige regelmaat op een scheepswerf in de
            regio waar mijn opa een boot aan het bouwen was. Mijn andere opa was
            ook een metaalbewerker en ook hem zag ik vaak lassen en werken met
            metaal. Misschien is de affiniteit met het vak hier onstaan maar dat
            is iets wat ik mezelf pas op latere leeftijd ben gaan realiseren.
          </Typography>

          <Typography variant="body1" component="p" paragraph>
            Per toeval en door samenloop van omstandigheden ben ik zelf op een
            scheepswerf komen te werken in mijn tienerjaren. Ondanks wat twijfel
            en een omscholing waarna ik het vak heb laten varen, ben ik
            uiteindelijk - na een kort vertrek uit de metaal van enkele jaren -
            toch weer heel hard terug gerent naar mijn oude beroep en heb ik
            mijn passie omarmt. Werken met metaal, dat doe ik elke dag met heel
            veel plezier.
          </Typography>

          <Typography variant="body1" component="p" paragraph>
            Uit die passie is mijn eenmanszaak geboren maar ook een VOF de Bruin
            en Paau industrial living. Door de combinatie van deze 2
            ondernemingen kan ik zowel de zakelijke als de particulieren klant
            op tal van fronten bedienen: van maatwerkmeubels tot werken op ZZP
            basis en van constructiewerk tot interieurdesign. Het opleveren van
            trappen tot tafels, van keukens tot overkappingen, hekwerken en
            maatwerken... U verzint het wij creëren het.
          </Typography>

          <Typography variant="body1" component="p" paragraph>
            Persoonlijk contact met de klant is voor mij belangrijk. Wat u
            verzint maak ik, binnen de perken van wat realistisch en mogelijk
            is. Daar denk ik graag met u over mee.
          </Typography>

          <Typography variant="body1" component="p" paragraph>
            Neem een kijkje bij mijn foto's; een kleine greep uit diversen
            handgemaakte producten. Als u meer wilt weten neem gerust contact
            op.
          </Typography>

          <Typography variant="body1" component="p" paragraph>
            Groeten, <br />
            Rick de Bruin
          </Typography>
        </Paper>
      </Container>
    </section>
  );
};

export default About;
