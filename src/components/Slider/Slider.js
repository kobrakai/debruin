import React from 'react'
import SlickSlider from "react-slick"
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'

import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

const Slider = () => {
    const getImageUrl = (image) => {
        return process.env.PUBLIC_URL + '/images/slider/' + image
    }

    const styles = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        color: 'white'
    }

    return (
        <section className="slider">
            <SlickSlider {...{
                dots: true,
                arrows: false,
                infinite: true,
                lazyload: true,
                adaptiveHeight: false,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1
            }}>
                <div>
                    <div style={{position: 'relative'}}>
                        <img src={getImageUrl('slide-1.jpg')} alt="slide 1"/>
                        <Container maxWidth="lg" style={styles}>
                            <Typography variant="h2">
                                Test content
                            </Typography>
                        </Container>
                    </div>
                </div>
                <div>
                    <div style={{position: 'relative'}}>
                        <img src={getImageUrl('slide-2.jpg')} alt="slide 2"/>
                        <Container maxWidth="lg" style={styles}>
                            <Typography variant="h2">
                                Test content
                            </Typography>
                        </Container>
                    </div>
                </div>
                <div>
                    <div style={{position: 'relative'}}>
                        <img src={getImageUrl('slide-3.jpg')} alt="slide 3"/>
                        <Container maxWidth="lg" style={styles}>
                            <Typography variant="h2">
                                Test content
                            </Typography>
                        </Container>
                    </div>
                </div>
                <div>
                    <div style={{position: 'relative'}}>
                        <img src={getImageUrl('slide-4.jpg')} alt="slide 4"/>
                        <Container maxWidth="lg" style={styles}>
                            <Typography variant="h2">
                                Test content
                            </Typography>
                        </Container>
                    </div>
                </div>
            </SlickSlider>
        </section>
    )
}

export default Slider
