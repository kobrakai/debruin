import React from 'react'
import Container from '@material-ui/core/Container'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

const Content = () => (
    <main className="content">
        <Container maxWidth="lg">
            <Paper style={{ padding: '30px', marginBottom: '30px', marginTop: '40px' }}>
                <Typography variant="body1" component="p" paragraph>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Quo molestiae voluptate veniam debitis! Nemo, fuga quasi.
                    Possimus officia aspernatur quae! Aperiam, sapiente ab aliquam sit delectus ullam ducimus suscipit vero.
                </Typography>

                <Typography variant="body1" component="p" paragraph>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit.
                    Quo molestiae voluptate veniam debitis! Nemo, fuga quasi.
                    Possimus officia aspernatur quae! Aperiam, sapiente ab aliquam sit delectus ullam ducimus suscipit vero.
                </Typography>
            </Paper>
        </Container>
    </main>
)

export default Content
