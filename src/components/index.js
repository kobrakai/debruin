import Header from './Header'
import Navigation from './Navigation'
import Slider from './Slider'
import Hero from './Hero'
import Content from './Content'
import About from './About'
import Contact from './Contact'
import Portfolio from './Portfolio'
import Footer from './Footer'

export {
    Header,
    Navigation,
    Slider,
    Hero,
    Content,
    About,
    Contact,
    Portfolio,
    Footer
}