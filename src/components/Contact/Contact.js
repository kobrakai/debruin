import React, { useState } from "react";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import { InputGroup, TextareaGroup, ContactBlock, Button } from "../../atoms";

const useStyles = makeStyles((theme) => ({
  contact: {
    position: "relative",
    margin: "5rem 0",
    "&::before": {
      content: '""',
      position: "absolute",
      top: 0,
      left: 0,
      right: 0,
      height: "100%",
      transform: "skewY(5deg)",
      backgroundImage: theme.palette.background.gradient,
      zIndex: -1,
    },
  },
}));

const Contact = (props) => {
  const classes = useStyles();
  const [isFilled, setIsFilled] = useState(false),
    [isValid, setIsValid] = useState(false),
    [formElements, setFormElements] = useState({
      name: {
        required: true,
        type: "text",
        placeholder: "",
        label: "Naam",
        valid: true,
        value: "",
        onBlur: (event) => handleBlur(event, "name"),
        onChange: (event) => handleChange(event, "name"),
      },
      phone: {
        required: false,
        type: "tel",
        placeholder: "",
        label: "Telefoonnummer",
        valid: true,
        value: "",
        onBlur: (event) => handleBlur(event, "phone"),
        onChange: (event) => handleChange(event, "phone"),
      },
      email: {
        required: true,
        type: "email",
        placeholder: "",
        label: "Email",
        valid: true,
        value: "",
        onBlur: (event) => handleBlur(event, "email"),
        onChange: (event) => handleChange(event, "email"),
      },
      message: {
        required: true,
        type: "textarea",
        placeholder: "",
        label: "Bericht",
        valid: true,
        value: "",
        onBlur: (event) => handleBlur(event, "message"),
        onChange: (event) => handleChange(event, "message"),
      },
    });

  const fields = Object.keys(formElements).map((key) => {
    const data = formElements[key],
      value = formElements[key].value;

    return data.type === "textarea" ? (
      <Grid item xs={12} key={key}>
        <TextareaGroup
          id={key}
          required={data.required}
          label={data.label}
          type={data.type}
          value={value}
          placeholder={data.placeholder}
          onChange={data.onChange}
        />
      </Grid>
    ) : (
      <Grid item xs={12} key={key}>
        <InputGroup
          id={key}
          required={data.required}
          label={data.label}
          type={data.type}
          value={value}
          valid={data.valid}
          message={data.message}
          placeholder={data.placeholder}
          onChange={data.onChange}
          onBlur={data.onBlur}
        />
      </Grid>
    );
  });

  let typingTimer = null;

  const handleChange = (event, field) => {
    const newField = formElements[field];
    newField.value = event.target.value;

    setFormElements({
      ...formElements,
      [field]: newField,
    });

    clearTimeout(typingTimer);
    typingTimer = setTimeout(checkFilled, 1000);
  };

  const handleBlur = (event, field) => {
    const newField = formElements[field];

    setFormElements({
      ...formElements,
      [field]: validateField(newField),
    });
  };

  const validateField = (field) => {
    switch (field.type) {
      case "email":
        const regdexMail = /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i;
        field.valid = !field.required || regdexMail.test(field.value);

        if (!field.required || field.valid) {
          field.message = false;
        } else {
          field.message = "Ongeldig emailadres";
        }

        break;
      case "tel":
        const regdexPhone = /^((\+|00(\s|\s?-\s?)?)31(\s|\s?-\s?)?(\(0\)[-\s]?)?|0)[1-9]((\s|\s?-\s?)?[0-9])((\s|\s?-\s?)?[0-9])((\s|\s?-\s?)?[0-9])\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]\s?[0-9]$/gm;
        field.valid = !field.required || regdexPhone.test(field.value);

        if (!field.required || field.valid) {
          field.message = false;
        } else {
          field.message = "Ongeldig telefoon nummer";
        }

        break;
      default:
        if (field.required) {
          field.valid = field.value.length > 0;

          if (field.valid) {
            field.message = false;
          } else {
            field.message = `${field.label} is verplicht`;
          }
        } else {
          field.valid = true;
          field.message = false;
        }
    }

    return field;
  };

  const checkFilled = () => {
    let foundEmpty = false;
    let foundInvalid = false;

    Object.keys(formElements).map((key) => {
      if (formElements[key].required && formElements[key].value === "") {
        foundEmpty = true;
      }

      if (formElements[key].required && formElements[key].valid === false) {
        foundInvalid = true;
      }

      return true;
    });

    setIsFilled(!foundEmpty);
    setIsValid(!foundInvalid);

    return !foundEmpty;
  };

  return (
    <section id="contact" className={classes.contact}>
      <Container maxWidth="lg">
        <Paper style={{ padding: "30px" }}>
          <Typography variant="h4" component="h3">
            Contact
          </Typography>

          <Grid container spacing={3} direction="row">
            <Grid item xs={12} sm={6} md={7} lg={8}>
              <form name="contact" method="post">
                <Grid container direction="row">
                  {fields}

                  <input type="hidden" name="form-name" value="contact" />

                  <Grid item xs={12}>
                    <Button
                      type="submit"
                      title="Verzenden"
                      disabled={!(isFilled && isValid)}
                    />
                  </Grid>
                </Grid>
              </form>
            </Grid>

            <Grid item xs={12} sm={6} md={5} lg={4}>
              <ContactBlock />
            </Grid>
          </Grid>
        </Paper>
      </Container>
    </section>
  );
};

export default Contact;
