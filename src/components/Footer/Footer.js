import React from 'react'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
    footer: {
        marginTop: '3rem',
        padding: '1rem 0',
        color: 'white',
        backgroundImage: theme.palette.background.gradient
    }
}))

const Footer = () => {
    const classes = useStyles(),
          year = new Date().getFullYear()

    return (
        <footer className={classes.footer}>
            <Container maxWidth="lg">
                <small>
                    &copy; Copyright {year}: {process.env.REACT_APP_NAME}
                </small>
            </Container>
        </footer>
    )
}

export default Footer
